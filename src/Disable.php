<?php

namespace Mytory\WpDisableRestApi;

use WP_Error;

/**
 * Class Disable
 */
class Disable
{
    public function __construct()
    {
        add_filter('rest_authentication_errors', [$this, 'disable']);
    }

    public function disable($result)
    {
        if ( ! empty($result)) {
            return $result;
        }
        if ( ! is_user_logged_in()) {
            return new WP_Error('rest_not_logged_in', 'You are not currently logged in.', array('status' => 401));
        }
        if ( ! current_user_can('administrator')) {
            return new WP_Error('rest_not_admin', 'You are not an administrator.', array('status' => 401));
        }

        return $result;
    }
}
