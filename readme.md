# 워드프레스 REST API 비활성화

wordpress의 rest api를 비활성화합니다. 일종의 보안 조처입니다.

이 패키지는 로그인하지 않은 사용자에 대해 그냥 rest api를 비활성화합니다.

자세한 방법은 다음 링크를 참고: https://wp-tutorials.tech/optimise-wordpress/disable-wordpress-rest-api-without-a-plugin/

